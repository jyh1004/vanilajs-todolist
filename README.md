# vanilajs-todolist

# 피드백
~~~
function foo1() {
    const testDom = document.querySelector('.test');
    ...
}

function foo2() {
    const testDom = document.querySelector('.test');
    ...
}
~~~

위의 코드처럼 개별 함수가 내부 querySelector를 실행하여 dom을 찾도록 구현했었다. 이렇게 구현한 이유는 다음과 같다.

- 전역으로 빼면 의도한 함수 외 다른 함수에서 조작할 수 있어서 위험성이 올라가기 때문에
- 보고 있는 함수 내에서 맥락을 파악하기 더 쉽기 때문에

하지만 위처럼 function 내에서 querySelector로 일일이 호출하면 아래와 같은 단점들이 존재한다.

- 곳곳에 퍼진 querySelector를 수정할 때 하나하나 일일이 찾아 수정해야 한다.(유지보수성 저하)
- 프로젝트 규모가 커지면 매번 querySelector를 호출함으로써 성능이 저하된다.

위와 같은 문제들을 해결하기 위해선 전역 변수로 한 번만 찾음으로써 해결할 수 있다. 하지만 다른 함수에서 조작할 수 있는 위험성이 증가하게 된다.

그러면 어떻게 해결해야 할까?

## 해결책1 - 중복되는 코드를 함수로 분리한다.
~~~
function getTestDom() {
    return document.querySelector('.test');
}

function foo1() {
    const testDom = getTestDom();
    ...
}

function foo2() {
    const testDom = getTestDom();
    ...
}
~~~

중복되는 코드를 함수로 분리함으로써 찾고자하는 dom요소의 식별자명이 변경되더라도 이에 유연하게 대응할 수 있다. 일일이 찾아서 식별자명을 수정해줄 필요가 없게된 것이다.

하지만, 함수가 유연하지 않고 외부적인 상황에 강하게 의존하게 된다. 즉 HTML 구조가 특정 구조로 되어있어야만 함수 내 쿼리셀렉터가 정상적으로 동작하게 된다. HTML 구조를 변경하려고 하면 함수 내부 수정이 필요하게 된다.

## 해결책2 - 쿼리셀렉터를 내부에서 실행하지 않고, 함수 외부에서 주입하게 한다.
~~~
function getTestDom() {
    return document.querySelector('.test');
}

function foo(testDom) {
    testDom.style.color = 'red';
    ...
}

function caller() {
    foo(getTestDom());
    ...
}
~~~

위의 코드처럼 쿼리셀렉터를 함수 외부에서 주입함으로써 foo라는 함수는 어떠한 요소든 color를 red로 변경할 수 있게 된다.

하지만, 매번 dom 요소를 인자로 보내줘야만 하는데 이는 실수의 여지가 존재한다.

## 해결책3 - 모듈화
~~~
function Obj(doms) {
    this.testDom = doms.testDom;
    this.testDom2 = doms.testDom2;
    this.foo();
}

Obj.prototype.foo = function() {
    this.testDom.style.color = 'red';
    ...
}

const obj = new Obj({
    testDom: document.querySelector('.test'),
    testDom2: document.getElementById('test2');
});
~~~

인자로 보내주어야 하는 dom들을 constructor로 받아 멤버변수로 보관한다.
그리고 obj가 수행해야 하는 책임과 역할을 객체의 메서드로 만든다.
obj의 메서드에서 멤버변수가 필요하면 this.xxx로 접근하면 된다.

> **Note**: 참고로 prototype을 안 써도 문제가 되진 않는다. javascript에서 모듈을 만드는 방법은 많다.

todo리스트 프로젝트에서 todoList를 모듈화하고 내부에서 addItem 메서드로 todo리스트에 todoItem을 추가하도록 하였다. 이 과정에서 todoItem은 독립적으로 작게 존재할 수 있어서 별도 파일로 작게 모듈화하였고, onToggle/ onDelete를 받도록 수정하였다. 

결과적으로 독립성을 가진 TodoListPage를 모듈화함으로써 위의 문제점을 해결할 수 있었다.
TodoListPage 객체 생성 시 dom 요소 몇개만 주입해주면 내부에서 정상적으로 동작하게 되고 확장성 있는 구조를 유지할 수 있게 된다. 이로 인해 todoList를 여러 개 생성할 수 있다.

# 새로 배우게 된 내용

## 1) querySelectorAll() 메서드의 리턴 타입은 배열이 아닌 유사 배열이다.
~~~
TodoListPage.prototype.getToggles = function () {
    return this.todoListDom.querySelectorAll('.toggle');
}

...

TodoListPage.prototype.updateAppFooter = function() {
    const todoItemToggles = Array.from(this.getToggles());

    // app-footer 활성화 여부
    if(todoItemToggles.length > 0) {
        this.appFooter.style.display = 'block';
    } else {
        this.appFooter.style.display = 'none';
    }

    ...
}
~~~

getToggles() 메서드는 querySelectorAll() 메서드로 '.toggle' 클래스 요소들을 전부 가져와 리턴하고 있다. 이는 `배열` 이 아닌 `유사 배열` 이기에 length 프로퍼티를 사용할 수 없다.

따라서, Array.from() 메서드로 유사배열을 배열로 변화하여 처리하였다.

## 2) 특정 요소에 hover프로퍼티를 설정할 때 자식의 css프로퍼티는 변경할 수 있지만 부모는 변경할 수 없다.
~~~
<li class="todo-item">
    <input class="toggle" type="checkbox">
    <label>todo1</label>
    <button class="destory">x</button>
</li>
~~~

`<li>` 태그에 hover시 button의 color 프로퍼티를 변경하고자 하는건 아래와 같이 속성을 줘서 가능하다.

~~~
li:hover > .destory {
    opacity: 1;
}
~~~

하지만, 반대로 button에 hover시 부모 요소인 li태그의 color를 변경하는건 불가능하다.
이는 javascript로 동적으로 해결할 수 있다.

## 3) 가상 선택자(::before 과 ::after 요소)의 프로퍼티를 동적으로 변경할 땐 javascript로 class를 동적으로 부여함으로써 해결할 수 있다.
~~~
.toggle-all + label:before {
    content: '❯';
    font-size: 22px;
    color: #e6e6e6;
    padding: 10px 27px 10px 27px;
}
~~~

위와 같은 가상 선택자 요소의 color를 동적으로 변경하고자 하였다. 쿼리셀렉터로 해당 요소를 가져오려 하였으나 이는 불가능함을 알게 되었다. 그래서 아래와 같이 해결하였다.

~~~
// label의 'selected' 클래스명이 부여될때의 css 스타일 추가
.toggle-all + label.selected:before {
    color: #737373;
}

// 쿼리셀레터로 해당 label가져온 후 'selected' 클래스를 동적으로 추가
const allCheckedBtn = document.querySelector('toggle-all + label');
label.setproperty('class', 'selected');
~~~

하지만 [의사 클래스 선택자](https://developer.mozilla.org/ko/docs/Web/CSS/Pseudo-classes)에 `:selected` 가 있음을 알게 되었다. 이를 활용해 체크박스가 선택되었을때의 프로퍼티를 별도로 설정해줄 수 있음을 알게되었다.

그래서 위의 코드들을 삭제하고 아래와 같이 css프로퍼티를 추가함으로써 깔끔하게 해결할 수 있었다.

~~~
.toggle-all:checked + label:before {
    color: #737373;
}
~~~

## 4) dom요소의 클래스를 조작할때 classList프로퍼티로 접근하자.

위의 예시처럼 `label.setproperty('class', 'selected');` 로 클래스를 설정하였다.
이는 기존 클래스를 전부 삭제하고 새로운 `selected` 클래스만 부여하게 된다.
`removeProperty('class')` 도 마찬가지였다. 기존 클래스를 전부 삭제한다.

dom요소의 [classList](https://developer.mozilla.org/ko/docs/Web/API/Element/classList) 를 사용하면 이를 해결할 수 있다. testDom.classList.add(), testDom.classList.remove(), testDom.classList.toggle() 과 같은 메서드를 사용하여 위와같은 문제를 해결할 수 있다.

> **Note**: dom요소의 id를 접근할때 `testDom.getAttribute('id')`와 같이 메소드 호출 없이 `testDom.id` 로 바로 접근하면 된다.

## 5) dom요소의 내부 텍스트를 변경시 textContent 프로퍼티를 사용하자.
`innerHTML`의 경우에는 [xss공격](https://noirstar.tistory.com/266)에 취약하다. 따라서 `textContent` 프로퍼티를 사용하자. 자세한 내용은 [여기](https://poiemaweb.com/js-dom#43-html-%EC%BD%98%ED%85%90%EC%B8%A0-%EC%A1%B0%EC%9E%91manipulation)를 참고하자.