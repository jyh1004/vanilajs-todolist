function TodoListPage(doms) {
    this.todoListDom = doms.todoListDom;
    this.todoInputDom = doms.todoInputDom;
    this.todoClearDom = doms.todoClearDom;
    this.filtersDom = doms.filtersDom;
    this.allCheckBtn = doms.allCheckBtn;
    this.allCheckLabel = doms.allCheckLabel;
    this.appFooter = doms.appFooter;
    this.todoCountDom = doms.todoCountDom;
    this.selectedFilter = this.filtersDom.querySelector('#filters-all'); // 현재 선택된 버튼을 참조하는 전역 변수

    this.initEvent();
}

/**
 * todo 항목을 추가한다.
 * @param {string} todoContents todo 항목 내용
 * @returns {void}
 */
 TodoListPage.prototype.addItem = function (todoContents) {
    var todoItem = TodoItemModule.create(
        todoContents,
        () => this.updateAppFooter(),
        (todoItem) => {
            this.deleteItem(todoItem);
        }
    );
    this.todoListDom.appendChild(todoItem);
    this.updateAppFooter();
};

/**
 * toggle 클래스를 가진 dom요소들을 리턴한다.
 * @returns {void}
 */
TodoListPage.prototype.getToggles = function () {
    return this.todoListDom.querySelectorAll('.toggle');
}

/**
 * todo 항목을 삭제한다.
 * @param {string} todoItem todo 항목을 나타내는 todo-item
 * @returns {void}
 */
 TodoListPage.prototype.deleteItem = function (todoItem) {
    this.todoListDom.removeChild(todoItem);
    this.updateAppFooter();
}

/**
 * filterId에 해당하는 필터 기능을 수행한다.
 * @param {string} filterId 적용할 필터 Id
 * @returns {void}
 */
 TodoListPage.prototype.doFilter = function(filterId) {
    if(filterId === 'filters-all') {
        this.showAll();
    } else if(filterId === 'filters-active') {
        this.showActive();
    } else {
        this.showCompleted();
    }
}

/**
 * 전체 등록 항목을 표시한다.
 * @returns {void}
 */
TodoListPage.prototype.showAll = function () {
    const todoItems = this.todoListDom.querySelectorAll('.toggle');
    for(let i=0; i<todoItems.length; i++) {
        todoItems[i].parentNode.style.display = 'block';
    }
}

/**
 * 미수행 항목을 표시한다.
 * @returns {void}
 */
 TodoListPage.prototype.showActive = function() {
    const todoItems = this.todoListDom.querySelectorAll('.toggle');
    for(let i=0; i<todoItems.length; i++) {
        if(todoItems[i].checked == true) {
            todoItems[i].parentNode.style.display = 'none';
        } else {
            todoItems[i].parentNode.style.display = 'block';
        }
    }
}

/**
 * 수행 완료 항목을 표시한다.
 * @returns {void}
 */
 TodoListPage.prototype.showCompleted = function() {
    const todoItems = this.todoListDom.querySelectorAll('.toggle');
    for(let i=0; i<todoItems.length; i++) {
        if(todoItems[i].checked == true) {
            todoItems[i].parentNode.style.display = 'block';
        } else {
            todoItems[i].parentNode.style.display = 'none';
        }
    }
}

/**
 * app-footer를 갱신한다.
 * @returns {void}
 */
 TodoListPage.prototype.updateAppFooter = function() {
    const todoItemToggles = Array.from(this.getToggles());

    // app-footer 활성화 여부
    if(todoItemToggles.length > 0) {
        this.appFooter.style.display = 'block';
    } else {
        this.appFooter.style.display = 'none';
    }

    // 잔여 항목 개수 체크
    const uncheckedCount = todoItemToggles.filter(toggle => toggle.checked == false).length;


    // 잔여 항목 수 갱신
    this.todoCountDom.textContent = uncheckedCount;

    // (잔여 항목 수에 따른) 완료 항목 삭제 버튼 활성화 여부
    if(uncheckedCount < todoItemToggles.length) {
        this.todoClearDom.style.display = 'block';
    } else {
        this.todoClearDom.style.display = 'none';
    }
    
    // (잔여 항목 수에 따른) 전체 체크 버튼 활성화 여부
    if (uncheckedCount == 0) {
        this.allCheckBtn.checked = true;
    } else {
        this.allCheckBtn.checked = false;
    }
}

/**
 * TodoListPage의 event를 초기화 한다.
 * @returns {void}
 */
TodoListPage.prototype.initEvent = function() {
    this.todoInputDom.addEventListener('keypress', (event) => {
        if (event.code === 'Enter') {
            if(this.todoInputDom.value.trim() !== '') {
                this.addItem(this.todoInputDom.value);
                this.todoInputDom.value = '';
            }
        }
    });

    this.todoClearDom.addEventListener('click', (event) => {
        const todoItems = this.getToggles();
        for(let i=0;i<todoItems.length; i++) {
            if(todoItems[i].checked == true) {
                this.deleteItem(todoItems[i].parentNode);
            }
        }
    });

    const filterBtns = this.filtersDom.querySelectorAll('li > a');
    filterBtns.forEach(filterBtn => 
        filterBtn.addEventListener('click', (event) => {
            const oldSelected = this.selectedBtn;
            const newSelected = filterBtn;

            const selectedFilterId = newSelected.getAttribute('id');
            this.doFilter(selectedFilterId);
            
            oldSelected.classList.remove('selected');
            newSelected.classList.add('selected');

            this.selectedBtn = newSelected;
        })
    );

    this.allCheckBtn.addEventListener('change', function(event) {
        const todoItems = this.getToggles();
        if(this.allCheckBtn.checked == true) {
            todoItems.forEach(item => item.checked = true);
        } else {
            todoItems.forEach(item => item.checked = false);
        }
        
        this.doFilter(this.selectedBtn.id);
        this.updateAppFooter();
    });
}

var page = new TodoListPage({
    todoListDom: document.querySelector('.todo-list'),
    todoInputDom: document.getElementById('todo-input'), 
    todoClearDom: document.querySelector('.clear-completed'),
    filtersDom: document.querySelector('.filters'),
    allCheckBtn: document.getElementById('toggle-all'),
    allCheckLabel: document.querySelector('#toggle-all + label'),
    appFooter: document.querySelector('.app-footer'),
    todoCountDom: document.querySelector('.todo-count > strong') 
});

page.addItem('todo1');
page.addItem('todo2');