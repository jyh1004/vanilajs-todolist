var TodoItemModule = (function() { 
    
    function createTodoItem(todoContents, onToggle, onDelete) {
        const todoItem = document.createElement('li');
        todoItem.setAttribute('class', 'todo-item');



        var label = createLabel(todoContents);
        var input = createTodoItemInput(onToggle);
        var delBtn = createTodoItemDelete(() => onDelete(todoItem));
        
        todoItem.appendChild(input);
        todoItem.appendChild(label);
        todoItem.appendChild(delBtn);

        return todoItem;
    }

    function createLabel(todoContents) {
        const label = document.createElement('label');
        const labelText = document.createTextNode(todoContents);
        label.appendChild(labelText);

        return label;
    }

    function createTodoItemInput(onToggle) {
        
        const input = document.createElement('input');
        input.setAttribute('class', 'toggle');
        input.setAttribute('type', 'checkbox');
        input.addEventListener('change', function(event) {
            onToggle();
        });
        
        return input;
    }

    function createTodoItemDelete(onDelete) {
        const delBtn = document.createElement('button');
        const btnText = document.createTextNode('x');
        delBtn.setAttribute('class', 'destory');
        delBtn.appendChild(btnText);
        delBtn.addEventListener('click', onDelete);

        return delBtn;
    }

    return {
        create: createTodoItem
    }
})();